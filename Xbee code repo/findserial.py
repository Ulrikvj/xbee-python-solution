import sys
import serial.tools.list_ports


class SerialFinder:
    def __init__(self):
        self.ports = serial.tools.list_ports.comports()
        self.serial_chosen = []
        self.ports_found = []

    def serial_available(self):
        if sys.platform.startswith('win'):
            print("OS - Windows")
        elif sys.platform.startswith('linux'):
            print("OS - Linux")
        try:
            baudrate = int(input("Enter desired baudrate: "))
            print("Serial ports available on this machine: ")
            for port, desc, hwid in sorted(self.ports):
                print(port)
                self.ports_found.append(port)

            port_chosen = int(input("Chose the desired serial port with indexing, starting from 0: "))
            self.serial_chosen.insert(0, baudrate)
            self.serial_chosen.insert(1, self.ports_found[port_chosen])
            return self.serial_chosen

        except IndexError:
            print("Remember to chose within the index range of serial ports found. With the first port being 0")
            print("Rerun program and try again")
            exit()

        except ValueError:
            print("Does not accept words. Enter baud rate using numbers")
            print("Rerun program and try again")
            exit()
