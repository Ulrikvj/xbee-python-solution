class WriteNodes:

    def __init__(self, node_list):
        self.xbee_nodes = node_list
        self.node_printer()

    def node_printer(self):

        f = open("Xbee_network_list.txt", "w")
        temp_numb = 0
        try:
            while True:
                temp = str(self.xbee_nodes[0 + temp_numb])  # Mac 0, 2, 4, 6, 8, 10 keeps adds by two
                f.write(temp.split("MAC ")[1])
                f.write(" ")
                temp_numb += 2
        except IndexError:
            f.close()
            print("All devices found in the Xbee network has been stored in the 'Xbee_network_list.txt' file")
